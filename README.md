# slot machine server protocol 協定整理

## Agenda

* [Header](#header)
* [7 登入訊息](#登入訊息)
* [8 用戶訊息](#用戶訊息)
* [500 遊戲訊息](#遊戲訊息)

---

## Header

封包頭總長為`20 Bytes`，示意圖如下：

    0                   1                   2                   
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |       |V|       |C|R| ps|UNI| |MCM|SCM|         
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |Message(payload) ......                        
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

其中：
* V：cbVersion
* C：cbCheckCode
* R：randNum
* UNI：uniqueNum
* trueCheckCode = (short) (cbCheckCode ^ (randNum ^ cbVersion));
* packSize = (ps) ^ (randNum ^ cbVersion ^ 0x1234);
    * packSize 是包含 header 的總長度 (20+payload size)。
    * cbVersion目前為定值0x14
---

## 心跳封包

* CMC： 0
* SCM：

    SCM     | Description
    ------- | -----------
    1       | receive heart beat。
    2       | heart beat back。

* Request Payload：

    ```
    No payload.
    ```
---

## 登入訊息

* CMC： 7
* SCM：

    SCM     | Description
    ------- | -----------
    7       | 帳戶登入。(request)
    109     | 帳戶登出。(request)
    106     | 帳戶登入成功。(response)
    107     | 帳戶登入失敗。(response)

* Request Payload：

    ```
    message LoginByID {
		optional int64 uid = 1; //用戶id 需由client端帶入
		optional string token = 2; //session id 由server端帶入 回傳給client
		optional string version = 3; //client端帶入
	};
    ```

* Response Payload：
    * 帳戶登入成功：

        ```
        message LoginInfo {
            required string version = 1;            //server版號
            optional int64 requestId = 2;           // 客户端此次请求的ID
        }
        ```

    * 帳戶登入失敗：

        ```
        message ErrorMessage {
            required sint32 code = 1; //错误代码
                                      // 1: 系統錯誤 詳細可看錯誤訊息
                                      // 1001: Token验证不通过
                                      // 1000: 玩家的資訊在另外一台server  請轉跳
                                      // 1002: 當前遊戲需要金幣不足
            optional string desc = 2; //错误消息
            optional string ip = 3;   //跳转的服务器ip (若code 為1000則會告訴client轉跳的server)
            optional int32 port = 4;  //跳转的端口 (若code 為1000則會告訴client轉跳的server)
            optional int32 serverType = 5; //跳转的服务器类型 (若code 為1000則會告訴client轉跳的server)
            optional int32 subType = 6;    //跳转的房间类型 (若code 為1000則會告訴client轉跳的server)
        }
        ```

    * 帳戶登出：

        ```
        No Payload.
        ```
---

## 用戶訊息

* CMC： 8
* SCM：

    SCM     | Description
    ------- | -----------
    7       | 進入遊戲。(request)
    10      | 離開遊戲。(request)
    1000    | 成功進入遊戲。(response)
    600     | 遊戲內出錯。(response)

* Request Payload：

    ```
     No Payload.
    ```

* Response Payload：
    * 成功進入遊戲：

        ```
		message RoomInfo {
	        repeated int32 betConfig = 1;           // 下注檔位
	        repeated int32 heroChose = 2;           // 玩家目前使用的英雄
	        repeated int32 heroOwns = 3;            // 玩家所擁有的英雄
	        repeated Hero heros = 4;                // 遊戲中所有的英雄
	        required int64 hpMax = 5;               // 英雄最大生命值
	        required CurrentState currentState = 6; // 玩家目前遊戲狀態
	        required Settle settle = 7;             // 玩家上一次遊戲執行spin的遊戲資訊
	        required int32 requestId = 8;           // 客户端此次请求的ID
        }
        ```
    * 成功離開遊戲：
    
        ```
         No Payload.
        ```

    * 遊戲內出錯：

        ```
        message ErrorMessage {
            required sint32 code = 1; //错误代码 
                                      // 1: 系統錯誤
                                      // 200: 進入遊戲出現錯誤，需看錯誤消息
            optional string desc = 2; //错误消息
            optional string ip = 3;   //跳转的服务器ip
            optional int32 port = 4;  //跳转的端口
            optional int32 serverType = 5; //跳转的服务器类型
            optional int32 subType = 6; //跳转的房间类型
        }
        ```

---

## 遊戲訊息

* CMC： 500
* SCM：

    SCM     | Description
    ------- | -----------
    1003    | 開始遊戲(spin)。(request)
    1004    | 確定Spin看完。(request)
	1005    | 英雄設定。(request)
    1013    | 回覆Spin。(response)
    1014    | 回覆確定Spin看完。(response)
	1015    | 回覆英雄設定。(response)
    600     | 遊戲內出錯。(response)

* Request Payload：
    * 開始遊戲(spin)：

        ```
        message StartReq {
            required int32 betScore = 1;
            repeated int32 assignDesk = 2;         // 可以讓QA指定牌面 for testing  
            required int64 requestId = 3;
        }
        ```
    * 確定Spin看完：

        ```
         No Payload.
        ```
		
	* 英雄設定：

        ```
        message HeroSetting{
	         repeated int32 heroChose = 1;          // 玩家所選擇的英雄
	         repeated int32 heroBuy = 2;            // 玩家所購買的英雄
	         required int64 requestId = 3;
        }

        ```

* Response Payload：
    * 回覆Spin：

    ```
	message Settle {   
        required int32 bet = 1;                 // 此spin下注金
        required bool isFinishSpin = 2;         // 玩家是否看完此spin動畫
        required int64 scoreDelta = 3;          // 此spin產生的獎金(+ or -)
        required int64 attackToEnemy = 4;       // 此spin對敵人的總攻擊值
        required int64 attackToHero = 5;        // 此spin對英雄們的總攻擊值
        required int32 heroTokenDelta = 6;      // 此spin所掉落的英雄碎片
        repeated int32 bossReward = 7;          // 此spin所獲得的BOSS獎金 [一王, 二王, 三王]
	    repeated RoundInfo roundInfos = 8;      // 此spin的攻擊歷程
        optional CurrentState nextState = 9;    // 下一次spin開始的初始狀態
        required int32 requestId = 10;          // 客户端此次请求的ID
    }
    ```

    * 確定Spin看完：

        ```
		 No Payload.
        ```
	* 確定英雄設定：

        ```
		 No Payload.
        ```
    * 遊戲內出錯：

		```
		message ErrorMessage {
			required sint32 code = 1; //错误代码 
									  // 1: 系統錯誤
									  // 200: 遊戲執行出現錯誤，需看錯誤消息
			optional string desc = 2; //错误消息
			optional string ip = 3;   //跳转的服务器ip
			optional int32 port = 4;  //跳转的端口
			optional int32 serverType = 5; //跳转的服务器类型
			optional int32 subType = 6; //跳转的房间类型
		}
		```

---

## 支付訊息

* CMC： 123
* SCM：

    SCM     | Description
    ------- | -----------

* Request Payload：

    ```
    ```

* Response Payload：

    ```
    ```

---

## 框架訊息

* CMC： 101
* SCM：

    SCM     | Description
    ------- | -----------

* Request Payload：

    ```
    ```

* Response Payload：

    ```
    ```

---
